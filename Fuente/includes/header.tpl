<!DOCTYPE html>
<html lang="en">
  <head>
      {block name=head}
    <meta charset="UTF-8">
    <title>PHP CRUD MYSQL</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <!-- FONT AWESOEM -->
      <!--<script src="../booststrap-3.3.7-dist/css/bootstrap.min.css"></script> -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link href="../booststrap-3.3.7-dist/css/bootstrap.min.css" ></link>
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

      <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

      {/block}
  </head>
  <body style="height:1500px">


  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
          <div class="navbar-header">
              <a class="navbar-brand" >Capacitacion</a>
          </div>
          <ul class="nav nav-tabs nav-justified">
              <li ><a href="index.php">Home</a></li>
              <li ><a href="../Modelo/tablaUsuarios.php">Usuario</a></li>
              <li ><a href="../Modelo/tablaContactos.php">Contacto</a></li>
          </ul>
      </div>
  </nav>
  <div class="container" style="margin-top:50px">

  </div>
  {block name=body}

  {/block}
  </body>
</html>