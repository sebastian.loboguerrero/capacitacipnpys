$(document).ready(function() {
    let arreglo_contacto = $('#arreglo_contacto').data("contactos");

    $('#dt_contacto').dataTable( {
        "pageLength": 8,
        "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
        data : arreglo_contacto,
        columns: [
            {"data" : "ID_USUARIO"},
            {"data" : "NOMBRE"},
            {"data" : "NUMERO"},
            {"data" : "TIPO_DE_NUMERO"},
            {"data" : "PARENTESCO"},
            {"mRender": function ( data, type, row ) {
                    return '<button class="editar btn btn-secondary" data-id-editar ='+ row["ID_CONTACTO"]+'>'+
                        '<span class="glyphicon glyphicon-pencil"></span>'+'</button>' +
                        '<button class="eliminar btn btn-danger" data-id-eliminar ='+ row["ID_CONTACTO"]+'>'+
                        '<span class="glyphicon glyphicon-trash"></span>'+'</button>';
                },
            }
            ],

    });

    $('.editar').click(function (){
        let idEditar = $(this).data("id-editar");
        window.location.href = "../Controlador/crearContacto.php?id="+idEditar
    });

    $('.eliminar').click(function (){
        let idEliminar = $(this).data("id-eliminar")
        Swal.fire({
            title: 'Seguro desea elimiar este contacto?',
            text: "No podra devulver esta decision!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar!'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire(
                    'eliminado!',
                    'Ya se elimino el Contacto .',
                    'success',
                    // redireccionarlo con un ajax al delet task .php
                    $.ajax({
                        type:'POST',
                        url:'../Modelo/deleteTaskContacto.php',
                        data:{'idEliminar': idEliminar },
                        success: function(data){
                            window.location.href = "../Modelo/tablaContactos.php"
                        },
                        error: function(data){
                            /*
                            * Se ejecuta si la peticón ha sido erronea
                            * */
                            alert("Faltan datos por llenar")
                        }
                    })
                )
            }
        })
    });


    //listar();
} );

