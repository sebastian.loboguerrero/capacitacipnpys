{extends file ="../includes/header.tpl"}
{block name=title}
    Usuario
{/block}
{block name=body}
    <script src="../js/tabla_usuario.js"> </script>
    <script src="../js/usuario_editar.js"></script>
    <p> </p>
    <center><h4>Tabla de Usuarios </h4></center>
    <div class="container p-4">
        <div class="col-md-15">
            <table id="grid" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                <tr>
                    <th hidden>Codigo</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Nacimiento</th>
                    <th>Genero</th>
                    <th>Modificar</th>
                </tr>
                </thead>
                <tbody>
                {foreach from = $arreglo_usuarios item = $usuario}
                    <tr>
                        <td hidden>{$usuario.ID_USUARIO}</td>
                        <td>{$usuario.NOMBRE}</td>
                        <td>{$usuario.APELLIDO}</td>
                        <td>{$usuario.NACIMIENTO}</td>
                        <td>{$usuario.GENERO}</td>
                        <td>
                            <button class="editar btn btn-secondary" data-id-editar={$usuario.ID_USUARIO}>
                                <span class="glyphicon glyphicon-pencil"></span>
                            </button>
                            <button class="eliminar btn btn-danger" data-id-eliminar={$usuario.ID_USUARIO}>
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </td>
                    </tr>

                {/foreach}

                </tbody>
            </table>
        </div>
        <div id="dialog"  hidden></div>
        <center>
            <div class="btn">
                <a class="btn btn-primary btn-block" href="../Controlador/crearUsuario.php" target="_blank">crear Usuario</a>
            </div>
        </center>
    </div>
{/block}


