
{extends file ="../includes/header.tpl"}
{block name=title}
    Contacto
{/block}
{block name=body}
    <script src="../js/contacto_editar.js"></script>
    <script src="../js/tabla_contacto.js"> </script>
    <p> </p>
    <input hidden id="arreglo_contacto" data-contactos={$arreglo_contacto}>

    <p> </p>
    <center><h4>Tabla de Contactos </h4></center>

    <div class="container p-4">
    <div class="col-md-15">
        <table id="dt_contacto" class="table table-striped table-bordered dt-responsive nowrap">
            <thead>
            <tr>
                <th hidden>Id Contato</th>
                <th>id Usuario</th>
                <th>Nombre</th>
                <th>Numero</th>
                <th>Tipo de Numero</th>
                <th>Parentesco</th>
                <th>Modificar</th>
            </tr>
            </thead>
            <tbody>
            <tr>




            </tr>
            </tbody>
        </table>
    </div>
        <div id="dialogContacto"  hidden></div>
        <center>
            <div class="btn">
                <a class="btn btn-primary btn-block" href="../Controlador/crearContacto.php" target="_blank">crear Contacto</a>
            </div>
        </center>
</div>
{/block}